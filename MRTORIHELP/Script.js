var Nsc4 = (document.layers);
var Ie4 = (document.all);

var win = this;
var n   = 0;

function seek_str(str) {
var txt, i, found;
  if (str == ""){
    alert("検索文字を入力して下さい。");
    return false;
  }
  if (Nsc4) {
    if (!win.find(str)){
      while(win.find(str, false, true)){
        n++;
      }
    }else{
      n++;
    }
    if (n == 0){alert(str + " は見つかりませんでした。");}
  }
  if (Ie4) {
    txt = win.document.body.createTextRange();
    for (i = 0; i <= n && (found = txt.findText(str)) != false; i++) {
      txt.moveStart("character", 1);
      txt.moveEnd("textedit");
    }
    if (found) {
      txt.moveStart("character", -1);
      txt.findText(str);
      txt.select();
      txt.scrollIntoView();
      n++;
    }else{
      if (n > 0) {
        n = 0;
        seek_str(str);
      }else{
        alert(str + " は見つかりませんでした。");
      }
    }
  }
  return false;
}
